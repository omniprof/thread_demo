package com.kenfogel.thread_demo;

/**
 * Responsive UI example. Modified to current standards
 *
 * @author Bruce Eckel From 'Thinking in Java, 3rd ed.' (c) Bruce Eckel 2002
 */
public class c10_ResponsiveUI extends Thread {

    private static volatile double d = 1;
    
    /**
     * Constructor that defines that this will be a daemon thread
     */
    public c10_ResponsiveUI() {
        setDaemon(true);
    }

    /**
     * The run method is called when this object is launched as a thread.
     */
    @Override
    public void run() {
        while (true) {
            d += (Math.PI + Math.E) / d;
            System.out.println(""+d);
        }
    }

}

