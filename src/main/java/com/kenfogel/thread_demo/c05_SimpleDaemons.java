package com.kenfogel.thread_demo;

/**
 * Simple Daemons example. Modified to current standards
 *
 * @author Bruce Eckel From 'Thinking in Java, 3rd ed.' (c) Bruce Eckel 2002
 */
public class c05_SimpleDaemons extends Thread {

    /**
     * Constructor that defines that this will be a daemon thread
     */
    public c05_SimpleDaemons() {
        setDaemon(true); // Must be called before start()
    }

    /**
     * The run method is called when this object is launched as a thread.
     */
    @Override
    public void run() {
        while (true) {
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println(this);
        }
    }
}

