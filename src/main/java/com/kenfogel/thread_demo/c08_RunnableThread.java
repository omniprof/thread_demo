package com.kenfogel.thread_demo;

/**
 * Runnable Threads example. Modified to current standards. Rather than extend
 * Thread you can create a thread from any class that implements the Runnable
 * interface. This is useful if you need to thread a class that already extends
 * another class.
 *
 * @author Bruce Eckel From 'Thinking in Java, 3rd ed.' (c) Bruce Eckel 2002
 */
public class c08_RunnableThread implements Runnable {

    private int countDown = 5;

    /**
     * Return info on the state of this thread
     *
     * @return thread name from the Thread supper class and thread count
     */
    @Override
    public String toString() {
        return "#" + Thread.currentThread().getName() + ": " + countDown;
    }

    /**
     * The run method is called when this object is launched as a thread.
     */
    @Override
    public void run() {
        while (true) {
            System.out.println(this);
            --countDown;
            if (countDown == 0) {
                return;
            }
        }
    }
}
