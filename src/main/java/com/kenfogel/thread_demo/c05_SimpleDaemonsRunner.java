package com.kenfogel.thread_demo;

import static java.lang.Thread.sleep;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Separate main from original example
 *
 * @author Ken Fogel
 */
public class c05_SimpleDaemonsRunner {

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(c05_SimpleDaemonsRunner.class);

    private static final int NUMBER_OF_THREADS = 10;

    /**
     * Where it all begins
     *
     * @param args
     */
    public static void main(String[] args) {
        c05_SimpleDaemonsRunner runner = new c05_SimpleDaemonsRunner();
        runner.perform();
    }

    /**
     * Create the threads that will run until the program exits
     */
    public void perform() {
        c05_SimpleDaemons[] c05 = new c05_SimpleDaemons[10];
        for (int i = 0; i < NUMBER_OF_THREADS; i++) {
            c05[i] = new c05_SimpleDaemons();
            c05[i].start();
        }
        try {
            sleep(1000);
        } catch (InterruptedException ex) {
            LOG.error("Interrupted Exception", ex);
        }
    }
}
