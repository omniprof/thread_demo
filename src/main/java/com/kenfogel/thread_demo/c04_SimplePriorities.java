package com.kenfogel.thread_demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simple Priorities example. Modified to current standards
 *
 * @author Bruce Eckel From 'Thinking in Java, 3rd ed.' (c) Bruce Eckel 2002
 */
public class c04_SimplePriorities extends Thread {

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(c04_SimplePrioritiesRunner.class);

    private int countDown = 5;
    private volatile double d = 0; // No optimization

    /**
     * The thread runner initializes this object with the priority
     *
     * @param priority
     */
    public c04_SimplePriorities(int priority) {
        //LOG.debug("Priority = " + priority);
        setPriority(priority);
    }

    /**
     * Display info on the state of this thread
     *
     * @return thread name from the Thread supper class and thread count
     */
    @Override
    public String toString() {
        return super.toString() + ": " + countDown;
    }

    /**
     * The run method is called when this object is launched as a thread.
     */
    @Override
    public void run() {
        while (true) {
            // An expensive, interruptable operation:
            for (int i = 1; i < 100000; i++) {
                d += (Math.PI + Math.E) / (double) i;
            }
            System.out.println(this);
            --countDown;
            if (countDown == 0) {
                return;
            }
        }
    }
}
