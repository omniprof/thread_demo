package com.kenfogel.thread_demo;

import java.io.IOException;

/**
 * UnResponsive UI example. Modified to current standards
 *
 * @author Bruce Eckel From 'Thinking in Java, 3rd ed.' (c) Bruce Eckel 2002
 */
public class c10_UnResponsiveUI {

    private static volatile double d = 1;

    /**
     * This method runs a loop that never ends so that the system will not
     * respond
     *
     * @throws IOException
     */
    public void runForEver() throws IOException {
        while (true) {
            d += (Math.PI + Math.E) / d;
            System.out.println("Unresponsive: " + d);
        }
    }
}
