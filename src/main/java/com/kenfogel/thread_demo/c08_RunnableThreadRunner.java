package com.kenfogel.thread_demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Separate main from original example
 *
 * @author Ken Fogel
 */
public class c08_RunnableThreadRunner  {

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(c05_SimpleDaemonsRunner.class);

    private static final int NUMBER_OF_THREADS = 5;

    /**
     * Where it all begins
     *
     * @param args
     */
    public static void main(String[] args) {
        c08_RunnableThreadRunner runner = new c08_RunnableThreadRunner();
        runner.perform();
    }

    /**
     * Create the runnable threads
     */
    public void perform() {
        Thread c08[] = new Thread[NUMBER_OF_THREADS];
        for (int i = 0; i < NUMBER_OF_THREADS; i++) {
            c08[i] = new Thread(new c08_RunnableThread(), "" + i);
            c08[i].start();
        }
    }
}

