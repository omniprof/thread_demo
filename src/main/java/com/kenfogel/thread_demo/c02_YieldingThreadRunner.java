package com.kenfogel.thread_demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Separate main from original example
 *
 * @author Ken Fogel
 */
public class c02_YieldingThreadRunner {

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(c02_YieldingThreadRunner.class);

    private static final int NUMBER_OF_THREADS = 5;

    /**
     * Where it all begins
     *
     * @param args
     */
    public static void main(String[] args) {
        c02_YieldingThreadRunner runner = new c02_YieldingThreadRunner();
        runner.perform();
    }

    /**
     * Create five threads
     */
    public void perform() {
        for (int i = 0; i < NUMBER_OF_THREADS; i++) {
            c02_YieldingThread c02 = new c02_YieldingThread(i);
            c02.start();
        }
    }
}
