package com.kenfogel.thread_demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Separate main from original example
 *
 * @author Ken Fogel
 */
public class c01_SimpleThreadRunner {

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(c01_SimpleThreadRunner.class);
    
    private static final int NUMBER_OF_THREADS = 5;
    
    /**
     * Where it all begins
     * @param args 
     */
    public static void main(String[] args) {
        c01_SimpleThreadRunner runner = new c01_SimpleThreadRunner();
        runner.perform();
    }

    /**
     * Create five threads
     */
    public void perform() {
        for (int i = 0; i < NUMBER_OF_THREADS; i++) {
            c01_SimpleThread c01 = new c01_SimpleThread(i);
            c01.start();
        }
    }
}
