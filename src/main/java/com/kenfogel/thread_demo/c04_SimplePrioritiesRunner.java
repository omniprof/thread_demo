package com.kenfogel.thread_demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Separate main from original example
 *
 * @author Ken Fogel
 */
public class c04_SimplePrioritiesRunner {

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(c04_SimplePrioritiesRunner.class);

    private static final int NUMBER_OF_THREADS = 5;

    /**
     * Where it all begins
     *
     * @param args
     */
    public static void main(String[] args) {
        c04_SimplePrioritiesRunner runner = new c04_SimplePrioritiesRunner();
        runner.perform();
    }

    /**
     * Create the threads, one with MAX_PRIORITY and the rest MIN_PRIORITY
     */
    public void perform() {
        c04_SimplePriorities[] c04 = new c04_SimplePriorities[NUMBER_OF_THREADS];
        c04[0] = new c04_SimplePriorities(Thread.MAX_PRIORITY);
        for (int i = 1; i < NUMBER_OF_THREADS; i++) {
            c04[i] = new c04_SimplePriorities(Thread.MIN_PRIORITY);
        }
        try {
            Thread.sleep(100);
        } catch (InterruptedException ex) {
            LOG.error("Interrupted Exception", ex);
        }
        for (int i = 0; i < NUMBER_OF_THREADS; i++) {
            c04[i].start();
        }
    }
}
