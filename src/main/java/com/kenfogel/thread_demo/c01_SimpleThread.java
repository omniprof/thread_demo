package com.kenfogel.thread_demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Very simple Threading example. Modified to current standards
 *
 * @author Bruce Eckel From 'Thinking in Java, 3rd ed.' (c) Bruce Eckel 2002
 */
public class c01_SimpleThread extends Thread {

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(c01_SimpleThread.class);

    private int countDown = 100;

    /**
     * The thread runner initializes this object with thread count
     *
     * @param threadCount
     */
    public c01_SimpleThread(int threadCount) {
        super("Thread #" + threadCount); // Store the thread name
    }

    /**
     * Display info on the state of this thread
     *
     * @return thread name from the Thread supper class and thread count
     */
    @Override
    public String toString() {
        return "#" + getName() + ": " + countDown;
    }

    /**
     * The run method is called when this object is launched as a thread.
     */
    @Override
    public void run() {
        while (true) {
            System.out.println(this);
            --countDown;
            if (countDown == 0) {
                return;
            }
        }
    }
}
