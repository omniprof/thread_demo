package com.kenfogel.thread_demo;

import java.io.IOException;
import static java.lang.Thread.sleep;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Separate main from original example
 *
 * @author Ken Fogel
 */
public class c10_ResponsiveUIRunner {

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(c10_ResponsiveUIRunner.class);

    /**
     * Where it all begins. Ignoring exceptions.
     *
     * @param args
     * @throws java.lang.InterruptedException
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws InterruptedException, IOException {
        c10_ResponsiveUIRunner runner = new c10_ResponsiveUIRunner();
        runner.perform();
    }

    /**
     * Create either a process that is unresponsive or with threading is
     * responsive. Ignoring exceptions.
     *
     * @throws java.lang.InterruptedException
     * @throws java.io.IOException
     */
    public void perform() throws InterruptedException, IOException {
        //c10_UnResponsiveUI c10 = new c10_UnResponsiveUI(); // Must kill this process
        //c10.runForEver();
        c10_ResponsiveUI c10 = new c10_ResponsiveUI();
        c10.start();
        Thread.sleep(300);
        System.in.read();
    }
}
